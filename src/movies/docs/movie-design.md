Movies Design
=======
* Author: Stewart Sabuka
* Date: 28 January 2023
* version: [v1.0.0]

Overview
-----------
Movie service that persist movies and allow users to list or search for movies, add new movies, update or delete existing movies 

## Collections
### Movies
```typescript
@Schema({ timestamps: true })
export class Movie {
  @Prop({ type: String, default: '', index: true })
  name: string;

  @Prop({ type: Number })
  year: number;

  @Prop({ type: [String], default: [] })
  genres: string[];

  @Prop({ type: Number })
  ageLimit: number;

  @Prop({ type: Number })
  rating: number;

  @Prop({ type: mongoose.Schema.Types.Mixed, default: [] })
  actors: any;

  @Prop({ type: mongoose.Schema.Types.Mixed, default: null })
  director: any;

  @Prop({ type: String, default: '' })
  synopsis: string;
}
```

## Module/Service
### Movie Module
The movie module will provide two routes `create`, `findAll`, `findOne`, `search`, `update`, `remove`

### POST /movie
- creates new movie
- returns new created movie

#### Req Body
```typescript
const body: CreateMovieDto = {
  name: String,
  year: Number,
  genres: Array,
  ageLimit: Number,
  rating: Number,
  actors: Array,
  director: Object,
  synopsis: String,
}
```
#### Res Body
```typescript
const body: Movie = {}
```

### GET /movie:id
-  get one movie
- upon request create new energy with default values if non exists
- return a movie

#### Req Param
```typescript
const id: String = '636a3d70dfbd9b469c1d7981'
```
#### Res Body
```typescript
const body: Movie = {}
```
### GET /movie?search=Stewart
-  search movies
- return list of movies match search criteria in `name`, `genres`, `actors`, `directors`

#### Req Param
```typescript
const search: String = 'Stewart'
```
#### Res Body
```typescript
const body: Movie[] = []
```

### PATCH /movie:id
-  update movie
- return updated movie

#### Req Param
```typescript
const id: String = '636a3d70dfbd9b469c1d7981'
```
#### Req Body
```typescript
const body: UpdateMovieDto = {}
```
#### Res Body
```typescript
const body: Movie[] = []
```

### DELETE /movie:id
-  delete movie
- return the number of deleted movies

#### Req Param
```typescript
const id: String = '636a3d70dfbd9b469c1d7981'
```

#### Res Body
```typescript
const body: Object = {}
```

Template references
===============

### ref
[web site]

[web site]: https://docs.nestjs.com
