export class CreateMovieDto {
  name: string;
  year: number;
  genres: Array<string>;
  ageLimit: number;
  rating: number;
  actors: Array<object>;
  director: any;
  synopsis: string;
}
