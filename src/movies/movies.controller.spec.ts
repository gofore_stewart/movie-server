import { Test, TestingModule } from '@nestjs/testing';
import { MoviesController } from './movies.controller';
import { MoviesService } from './movies.service';
import { Model } from 'mongoose';
import { Movie, MovieDocument } from './schemas/movie.schema';
import { getConnectionToken, getModelToken } from '@nestjs/mongoose';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';

describe('MoviesController', () => {
  let controller: MoviesController;
  let service: MoviesService;
  let mockMovieModel: Model<MovieDocument>;
  const movie = new Movie();
  const movieId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MoviesController],
      providers: [
        MoviesService,
        {
          provide: getConnectionToken('Database'),
          useValue: {},
        },
        {
          provide: getModelToken(Movie.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockMovieModel = module.get<Model<MovieDocument>>(
      getModelToken(Movie.name),
    );
    service = module.get<MoviesService>(MoviesService);
    controller = module.get<MoviesController>(MoviesController);
  });

  it('(MovieController) should be defined', () => {
    expect(controller).toBeDefined();
  });
  it('(MovieService) should be defined', () => {
    expect(service).toBeDefined();
  });
  it('(MovieModel) should be defined', () => {
    expect(mockMovieModel).toBeDefined();
  });
  it('(Movie) should be defined', () => {
    expect(movie).toBeDefined();
  });
  it('should create movie', async () => {
    const obj: CreateMovieDto = {
      name: 'The Departed',
      year: 2006,
      genres: ['Crime', 'Drama', 'Thriller'],
      ageLimit: 13,
      rating: 8,
      actors: [
        {
          firstName: 'Leonardo',
          lastName: 'DiCaprio',
        },
        {
          firstName: 'Matt',
          lastName: 'Damon',
        },
        {
          firstName: 'Jack',
          lastName: 'Nicholson',
        },
      ],
      director: {
        firstName: 'Martin',
        lastName: 'Scorsese',
      },
      synopsis:
        'An undercover cop and a mole in the police attempt to identify each other while infiltrating an Irish gang in South Boston',
    };
    const spy = jest
      .spyOn(controller, 'create')
      .mockResolvedValue(movie as MovieDocument);
    await controller.create(obj);
    expect(spy).toBeCalled();
    expect(await controller.create(obj)).toStrictEqual(movie);
  });

  it('should update movie', async () => {
    const obj: UpdateMovieDto = {
      name: 'Time Travel',
      year: 2009,
    };
    const spy = jest
      .spyOn(controller, 'update')
      .mockResolvedValue(movie as MovieDocument);
    await controller.update(movieId, obj);
    expect(spy).toBeCalled();
    expect(await controller.update(movieId, obj)).toStrictEqual(movie);
  });

  it('should remove movie', async () => {
    const spy = jest
      .spyOn(controller, 'remove')
      .mockResolvedValue(movie as MovieDocument);
    await controller.remove(movieId);
    expect(spy).toBeCalled();
    expect(await controller.remove(movieId)).toStrictEqual(movie);
  });
});
