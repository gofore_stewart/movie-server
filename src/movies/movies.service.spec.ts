import { Test, TestingModule } from '@nestjs/testing';
import { MoviesService } from './movies.service';
import { Model } from 'mongoose';
import { Movie, MovieDocument } from './schemas/movie.schema';
import { getModelToken } from '@nestjs/mongoose';
import { UpdateMovieDto } from './dto/update-movie.dto';

describe('MoviesService', () => {
  let service: MoviesService;
  let mockMovieModel: Model<MovieDocument>;
  const movie = new Movie();
  const movieId = '636a3d70dfbd9b469c1d7981';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MoviesService,
        {
          provide: getModelToken(Movie.name),
          useValue: Model,
        },
      ],
    }).compile();
    mockMovieModel = module.get<Model<MovieDocument>>(
      getModelToken(Movie.name),
    );
    service = module.get<MoviesService>(MoviesService);
  });

  it('(MovieService) should be defined', () => {
    expect(service).toBeDefined();
  });

  it('(MovieModel) should be defined', () => {
    expect(mockMovieModel).toBeDefined();
  });

  it('should get one movie', async () => {
    const spy = jest
      .spyOn(service, 'findOne')
      .mockResolvedValue(movie as MovieDocument);
    await service.findOne(movieId);
    expect(spy).toBeCalled();
    expect(await service.findOne(movieId)).toBe(movie);
  });

  it('should find all movies', async () => {
    const spy = jest
      .spyOn(service, 'findAll')
      .mockResolvedValue([movie] as MovieDocument[]);
    await service.findAll();
    expect(spy).toBeCalled();
    expect(await service.findAll()).toStrictEqual([movie]);
  });

  it('should update movies', async () => {
    const obj: UpdateMovieDto = {};
    const spy = jest
      .spyOn(service, 'update')
      .mockResolvedValue(movie as MovieDocument);
    await service.update(movieId, obj);
    expect(spy).toBeCalled();
    expect(await service.update(movieId, obj)).toBe(movie);
  });

  it('should remove movies', async () => {
    const spy = jest
      .spyOn(service, 'remove')
      .mockResolvedValue(movie as MovieDocument);
    await service.remove(movieId);
    expect(spy).toBeCalled();
    expect(await service.remove(movieId)).toBe(movie);
  });

  it('should search movies', async () => {
    const query = 'Stewart';
    const spy = jest
      .spyOn(service, 'search')
      .mockResolvedValue([movie] as MovieDocument[]);
    await service.search(query);
    expect(spy).toBeCalled();
    expect(await service.search(query)).toStrictEqual([movie]);
  });
});
