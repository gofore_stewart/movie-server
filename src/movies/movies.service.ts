import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { Movie, MovieDocument } from './schemas/movie.schema';

@Injectable()
export class MoviesService {
  constructor(
    @InjectModel(Movie.name) private movieModel: Model<MovieDocument>,
  ) {}
  async create(createdMovieDto: CreateMovieDto): Promise<Movie> {
    const createdMovie = new this.movieModel(createdMovieDto);
    return createdMovie.save();
  }

  async findAll(): Promise<Movie[]> {
    return this.movieModel.find().lean().exec();
  }

  async findOne(id: string): Promise<Movie | null> {
    return this.movieModel.findOne({ _id: id }).lean().exec();
  }

  async find(query: Record<string, any>): Promise<Movie[]> {
    return this.movieModel.find(query).lean().exec();
  }

  async search(query: string): Promise<Movie[]> {
    return this.movieModel
      .find({
        $or: [
          { name: new RegExp(query, 'i') },
          { 'director.firstName': new RegExp(query, 'i') },
          { 'director.lastName': new RegExp(query, 'i') },
          { genres: { $in: [new RegExp(query, 'i')] } },
          { 'actors.firstName': new RegExp(query, 'i') },
          { 'actors.lastName': new RegExp(query, 'i') },
        ],
      })
      .lean()
      .exec();
  }

  async update(
    id: string,
    updateMovieDto: UpdateMovieDto,
  ): Promise<Movie | null> {
    return this.movieModel.findOneAndUpdate({ _id: id }, updateMovieDto, {
      new: true,
    });
  }

  async remove(id: string): Promise<Movie | null> {
    return await this.movieModel.findByIdAndRemove({ _id: id }).exec();
  }
}
