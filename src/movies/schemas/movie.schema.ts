import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type MovieDocument = Movie & Document;

@Schema({ timestamps: true })
export class Movie {
  @Prop({ type: String, default: '', index: true })
  name: string;

  @Prop({ type: Number })
  year: number;

  @Prop({ type: [String], default: [] })
  genres: string[];

  @Prop({ type: Number })
  ageLimit: number;

  @Prop({ type: Number })
  rating: number;

  @Prop({ type: mongoose.Schema.Types.Mixed, default: [] })
  actors: any;

  @Prop({ type: mongoose.Schema.Types.Mixed, default: null })
  director: any;

  @Prop({ type: String, default: '' })
  synopsis: string;
}

export const MovieSchema = SchemaFactory.createForClass(Movie);
